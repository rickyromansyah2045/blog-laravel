<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index() {
        return view('register');
    }

    public function welcome(Request $request) {
        
        $firstname      = $request['fname'];
        $lastname       = $request['lname'];
        $jenisKelamin   = $request['fav_language'];
        $kebangsaan     = $request['nationality'];
        $pesan          = $request['message'];
        return view('welcome', compact('firstname', 'lastname'));
    }
}
