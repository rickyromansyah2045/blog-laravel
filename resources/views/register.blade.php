@extends('layout.master')

@section('judul')
    Halaman Form Register
@endsection

@section('content')

    <div>
        <h1>Buat Account Baru!</h1>
        <h3><b>Sign Up Form</b></h3>
    </div>
 
    <form action="/welcome" method="post">
        @csrf
        <label for="fname">First Name: </label><br><br>
        <input type="text" id="fname" name="fname"><br><br>

        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname"><br><br>

        <label for="">Gender: </label><br><br>
        <input type="radio" id="male" name="fav_language" value="MALE">
        <label for="male">Male</label><br>

        <input type="radio" id="female" name="fav_language" value="FEMALE">
        <label for="female">Female</label><br>

        <input type="radio" id="other" name="fav_language" value="OTHER">
        <label for="other">Other</label><br><br>

        <label for="nationality">Nationality :</label><br><br>

        <select id="nationality" name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select> <br>

        <p>Language Spoken</p>
        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bahasa Indonesia">
        <label for="vehicle1">Bahasa Indonesia</label><br>
        <input type="checkbox" id="vehicle2" name="vehicle2" value="English">
        <label for="vehicle2">English</label><br>
        <input type="checkbox" id="vehicle3" name="vehicle3" value="Other">
        <label for="vehicle3">Other</label><br>

        <p>Bio :</p>
        <textarea name="message" id="message" cols="30" rows="10">
        </textarea><br><br>

        <!-- <a href="welcome.html"><button>Sign Up</button></a> -->
        <input type="submit" value="Sign Up">
    
    </form>
    @endsection
